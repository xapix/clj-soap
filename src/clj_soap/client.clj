(ns clj-soap.client
  (:require [clojure.tools.logging :as log]
            [clojure.data.xml :as xml]
            [clj-time.core :as t]
            [clj-time.format :as f])
  (:import [org.apache.axis2.client ServiceClient
                                    Options]
           [org.apache.axis2.addressing EndpointReference]
           [org.apache.axis2.transport.http HTTPConstants
                                            HTTPAuthenticator]
           [org.apache.axis2.transport.http.impl.httpclient3 HttpTransportPropertiesImpl$Authenticator]
           [org.apache.axiom.om OMAbstractFactory OMElement]
           [org.apache.axis2.description OutOnlyAxisOperation
                                         AxisService AxisOperation AxisMessage]
           [javax.xml.namespace QName]
           [java.net URL
                     Authenticator
                     PasswordAuthentication]
           [java.util ArrayList Iterator]
           [org.apache.axis2.context NamedValue]
           (org.apache.ws.commons.schema XmlSchemaComplexType XmlSchemaSequence XmlSchemaElement)
           (org.apache.axiom.om.impl.common.factory OMFactoryImpl)))

(defn axis-service-operations
  [^AxisService axis-service]
  (iterator-seq (.getOperations axis-service)))

(defn axis-op-name
  [^AxisOperation axis-op]
  (some-> axis-op
          (.getName)
          (.getLocalPart)))

(defn axis-op-namespace
  [^AxisOperation axis-op]
  (some-> axis-op
          (.getMessage "Out")
          (.getElementQName)
          (.getNamespaceURI)))

(defn axis-op-args
  [^AxisOperation axis-op]
  (for [^XmlSchemaElement elem (some-> ^AxisMessage (first (filter (fn [^AxisMessage msg] (= "out" (.getDirection msg)))
                                                                   (iterator-seq (.getMessages axis-op))))
                                       (.getSchemaElement)
                                       ^XmlSchemaComplexType (.getSchemaType)
                                       ^XmlSchemaSequence (.getParticle)
                                       (.getItems)
                                       (seq))]
    {:name (or (.getName elem) (some-> elem .getWireName .getLocalPart))
     :type (some-> elem .getSchemaType .getName keyword)
     :elem elem}))

(defn make-om-elem
  (^OMElement [^OMFactoryImpl factory tag-name]
   {:pre [(or (instance? QName tag-name)
              (string? tag-name))]}
   (let [^QName qname (if (instance? QName tag-name) tag-name (QName. tag-name))]
     (.createOMElement factory qname)))
  (^OMElement [factory tag-name value]
   (doto (make-om-elem factory tag-name)
     (.setText (str value)))))

(defn parameter->om-element
  [factory op arg-key arg-val]
  (let [tag-name (QName. (axis-op-namespace op)
                         (name arg-key))]
    (cond
      ;; We don't support lists here yet
      (sequential? arg-val)
      (throw (UnsupportedOperationException. "XML Arrays not yet supported"))

      ;; Maps are resolved recursively
      (map? arg-val)
      (let [outer-element (make-om-elem factory tag-name)]
        (doseq [[el-key el-val] arg-val]
          (.addChild outer-element
                     (parameter->om-element factory op el-key el-val)))
        outer-element)

      ;; Everything else will be added as a tag
      :else
      (make-om-elem factory tag-name arg-val))))

(defn add-request-parameter!
  "Adds a single parameter to the request."
  [op ^OMElement request factory arg-key arg-val]
  (.addChild request
             (parameter->om-element factory op arg-key arg-val)))

(defn make-request
  [op args]
  (let [factory (OMAbstractFactory/getOMFactory)
        request (make-om-elem factory (QName. (axis-op-namespace op) (axis-op-name op)))
        op-args (axis-op-args op)]

    ;; Some APIs require the parameters to appear in the same order they are defined in the WSDL.
    ;; Hence we're looping through the defined params first,
    ;; then we add all parameters that aren't defined in the WSDL.

    ;; Handle all WSDL-defined parameters
    (doseq [arg-type op-args]
      (let [arg-key (:name arg-type)
            arg-val ((keyword arg-key) args)]
        (add-request-parameter! op request factory arg-key arg-val)))

    ;; Handle all remaining parameters
    (let [op-args-keys         (map #(keyword (:name %)) op-args)
          args-without-defined (apply dissoc args op-args-keys)]
      (doseq [[arg-key arg-val] args-without-defined]
        (add-request-parameter! op request factory arg-key arg-val)))

    ;; Return request element
    request))

(defn parse-response
  [response]
  (let [response-str (str response)]
    (log/trace "SOAP Operation Response: " response-str)
    (xml/parse-str response-str)))

(defn client-call
  [^ServiceClient client ^AxisOperation op options args]
  (let [^OMElement request (make-request op args)]
    (locking client
      (log/trace "Invoking SOAP Operation '" (.getName op) "' with payload: " (.toString request))
      (if (isa? (class op) OutOnlyAxisOperation)
        (.sendRobust client (.getName op) request)
        (parse-response
          (.sendReceive client (.getName op) request))))))

(defn client-proxy
  [^ServiceClient client options]
  (->> (for [op (axis-service-operations (.getAxisService client))]
         [(keyword (axis-op-name op))
          (fn [args] (client-call client op options args))])
       (into {})))

(defn make-client
  [url & [{:keys [auth throw-faults timeout chunked? wsdl-auth headers endpoint-url]
           :or   {throw-faults true
                  chunked?     false}}]]
  (let [options (doto (Options.)
                  (.setTo (EndpointReference. url))
                  (.setProperty HTTPConstants/CHUNKED (str chunked?))
                  (.setExceptionToBeThrownOnSOAPFault throw-faults))]

    ;; if WSDL is password-protected, must enable access for URLConnection/connect
    ;; which is used internally by Axis2
    (when wsdl-auth
      (let [url-authenticator (proxy [Authenticator] []
                                (getPasswordAuthentication []
                                  (PasswordAuthentication. (:username wsdl-auth)
                                                           (char-array (:password wsdl-auth)))))]
        (Authenticator/setDefault url-authenticator)))

    ;; support authentication when making SOAP requests
    (when auth
      (let [req-authenticator (doto (HttpTransportPropertiesImpl$Authenticator.)
                                (.setUsername (:username auth))
                                (.setPassword (:password auth)))]
        (.setProperty options HTTPConstants/AUTHENTICATE req-authenticator)))

    ;; enable connection timeouts
    (when timeout
      (let [timeout-int (int timeout)]
        (doto options
          (.setTimeOutInMilliSeconds timeout-int)
          (.setProperty HTTPConstants/SO_TIMEOUT timeout-int)
          (.setProperty HTTPConstants/CONNECTION_TIMEOUT timeout-int))))

    ;; enable custom headers
    (when headers
      (let [headers-list (ArrayList.)]
        (doseq [[k v] headers]
          (.add headers-list (NamedValue. (str k) (str v))))
        (.setProperty options HTTPConstants/HTTP_HEADERS headers-list)))

    (let [axis-service (AxisService/createClientSideAxisService (URL. url) nil nil options)]

      ;; enable custom endpoint url -  we need to alter this here,
      ;; as `createClientSideAxisService` will reset `options.to` to the WSDL default.
      (when endpoint-url
        (.setTo options (EndpointReference. endpoint-url)))

      (doto (ServiceClient. nil axis-service)
        (.setOverrideOptions options)))))

(defn client-fn
  "Creates SOAP client proxy function which must be invoked with keywordized
  version of the SOAP function and any additional arguments as keywordized hash.
  e.g. (client :GetData {:foo \"test1\" :bar \"test2\"}).
  A map of options are required for generating the function.
  Either :base-client must be supplied (created with make-client) or the :wsdl
  URL string with :options data."
  [{:keys [wsdl options base-client]}]
  (let [; either base client must be supplied or URL with optional data
        client (or base-client (make-client wsdl options))
        px     (client-proxy client options)]
    (fn [method-name args]
      (if-let [operation (px method-name)]
        (operation args)
        (throw (IllegalArgumentException. (str "Cannot find SOAP method '" method-name "'. "
                                               "Available methods are: " (keys px))))))))
