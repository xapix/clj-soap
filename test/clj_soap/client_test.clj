(ns clj-soap.client-test
  (:require [clojure.test :refer :all]
            [clj-soap.client :as soap-client]
            [clojure.java.io :as io])
  (:import (org.apache.axiom.om OMAbstractFactory)
           (javax.xml.namespace QName)
           (org.apache.axis2.transport.http HTTPConstants)))

;;;
;;; Helpers
;;;

(def wsdl-test (-> "test-service.wsdl"
                   (io/resource)
                   (str)))

(defn get-axis-service-operations
  [wsdl]
  (-> wsdl
      (soap-client/make-client)
      (.getAxisService)
      (soap-client/axis-service-operations)))


;;;
;;; Tests
;;;

(deftest axis-service-operations-test
  (is (= 1
         (count (get-axis-service-operations wsdl-test)))))

(deftest axis-op-name-test
  (is (= ["fight"]
         (->> wsdl-test
              (get-axis-service-operations)
              (map #(soap-client/axis-op-name %))))))

(deftest axis-op-namespace-test
  (is (= ["http://example.org/pokemon"]
         (->> wsdl-test
              (get-axis-service-operations)
              (map #(soap-client/axis-op-namespace %))))))

(deftest axis-op-args-test
  (is (= [[{:name "Pokemon-1", :type :string} {:name "Pokemon-2", :type :string}]]
         (->> wsdl-test
              (get-axis-service-operations)
              (map #(->> %
                         (soap-client/axis-op-args)
                         ;; we need to remove elem to make it comparable, b/c it contains a Java object.
                         (map (fn [el] (dissoc el :elem)))))))))

(deftest make-om-elem-test
  (are [args expected-result]
    (= expected-result
       (-> (OMAbstractFactory/getOMFactory)
           (#(apply soap-client/make-om-elem % args))
           (.toString)))

    ;; Empty Tag (String)
    ["foo"]
    "<foo/>"

    ;; Empty Tag (QName)
    [(QName. "http://www.example.com/" "foo")]
    "<nsnErhx:foo xmlns:nsnErhx=\"http://www.example.com/\"/>"

    ;; Tag, nil value
    ["foo" nil]
    "<foo/>"

    ;; Tag, nil value and type
    ["foo" nil]
    "<foo/>"

    ;; Tag with value
    ["foo" "bar"]
    "<foo>bar</foo>"

    ;; Tag with value, nil type
    ["foo" "bar"]
    "<foo>bar</foo>"))

(deftest parameter->om-element-test
  (let [factory (OMAbstractFactory/getOMFactory)
        op      (-> wsdl-test
                    (get-axis-service-operations)
                    (first))]
    (testing "throws on sequence"
      (is (thrown? UnsupportedOperationException
                   (soap-client/parameter->om-element factory op :foo [1 2 3])))
      (is (thrown? UnsupportedOperationException
                   (soap-client/parameter->om-element factory op :foo {:name "Pikachu", :type ["Electric"]}))))
    (testing "Accepts Values"
      (are [input-map result]
        (= result
           (->> input-map
                (soap-client/parameter->om-element factory op :pokemon)
                (.toString)))

        ;; Plain Value
        "yes, please."
        "<nsjaeUG:pokemon xmlns:nsjaeUG=\"http://example.org/pokemon\">yes, please.</nsjaeUG:pokemon>"

        ;; Flat Map
        {:name "Pikachu", :type "Electric"}
        "<nsjaeUG:pokemon xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:name>Pikachu</nsjaeUG:name><nsjaeUG:type>Electric</nsjaeUG:type></nsjaeUG:pokemon>"

        ;; 1 level map
        {:name "Pikachu", :type "Electric", :weight {:kg 6.0, :lb 13.2}}
        "<nsjaeUG:pokemon xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:name>Pikachu</nsjaeUG:name><nsjaeUG:type>Electric</nsjaeUG:type><nsjaeUG:weight><nsjaeUG:kg>6.0</nsjaeUG:kg><nsjaeUG:lb>13.2</nsjaeUG:lb></nsjaeUG:weight></nsjaeUG:pokemon>"

        ;; 2+ level map
        {:name "Pikachu", :type "Electric", :breeding {:weight {:kg 6.0, :lb 13.2}, :height {:meter 0.4 :inch 15.74}}}
        "<nsjaeUG:pokemon xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:name>Pikachu</nsjaeUG:name><nsjaeUG:type>Electric</nsjaeUG:type><nsjaeUG:breeding><nsjaeUG:weight><nsjaeUG:kg>6.0</nsjaeUG:kg><nsjaeUG:lb>13.2</nsjaeUG:lb></nsjaeUG:weight><nsjaeUG:height><nsjaeUG:meter>0.4</nsjaeUG:meter><nsjaeUG:inch>15.74</nsjaeUG:inch></nsjaeUG:height></nsjaeUG:breeding></nsjaeUG:pokemon>"))))

(deftest make-request-test
  (let [op (-> wsdl-test
               (get-axis-service-operations)
               (first))]
    (are [args expected-result]
      (= expected-result
         (->> args
              (soap-client/make-request op)
              (.toString)))

      ;; Empty options
      {}
      "<nsjaeUG:fight xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:Pokemon-1/><nsjaeUG:Pokemon-2/></nsjaeUG:fight>"

      ;; Required options
      {:Pokemon-1 "Pikachu", :Pokemon-2 "Magikarp"}
      "<nsjaeUG:fight xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:Pokemon-1>Pikachu</nsjaeUG:Pokemon-1><nsjaeUG:Pokemon-2>Magikarp</nsjaeUG:Pokemon-2></nsjaeUG:fight>"

      ;; Optional options
      {:my-favorite-pokemon "Pikachu"}
      "<nsjaeUG:fight xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:Pokemon-1/><nsjaeUG:Pokemon-2/><nsjaeUG:my-favorite-pokemon>Pikachu</nsjaeUG:my-favorite-pokemon></nsjaeUG:fight>"

      ;; Required and optional options
      {:Pokemon-1 "Pikachu", :Pokemon-2 "Magikarp", :pokemons {:my-favorite "Pikachu", :also-good "Magikarp"}}
      "<nsjaeUG:fight xmlns:nsjaeUG=\"http://example.org/pokemon\"><nsjaeUG:Pokemon-1>Pikachu</nsjaeUG:Pokemon-1><nsjaeUG:Pokemon-2>Magikarp</nsjaeUG:Pokemon-2><nsjaeUG:pokemons><nsjaeUG:my-favorite>Pikachu</nsjaeUG:my-favorite><nsjaeUG:also-good>Magikarp</nsjaeUG:also-good></nsjaeUG:pokemons></nsjaeUG:fight>")))

(deftest make-client-test
  (testing "Setting custom headers"
    (is (= "[Name =Foo      Value =Bar]"
           (-> (soap-client/make-client wsdl-test {:headers {"Foo" "Bar"}})
               (.getOverrideOptions)
               (.getProperty HTTPConstants/HTTP_HEADERS)
               (.toString)))))
  (testing "Setting auth headers"
    (is (= "Foo:Bar"
           (-> (soap-client/make-client wsdl-test {:auth {:username "Foo"
                                                          :password "Bar"}})
               (.getOverrideOptions)
               (.getProperty HTTPConstants/AUTHENTICATE)
               (#(str (.getUsername %) ":" (.getPassword %)))))))
  (testing "Setting Timeout"
    (is (= "0"
           (-> (soap-client/make-client wsdl-test {:timeout 0})
               (.getOverrideOptions)
               (.getProperty HTTPConstants/CONNECTION_TIMEOUT)
               (.toString))))))


;;;
;;; Test complete requests
;;;

(deftest test-client-request
  (testing "Pokemon "
    (let [client (soap-client/client-fn {:wsdl    "http://mock-api-service:8080/castlemock/mock/soap/project/ZFgj7E/PokemonSOAP?wsdl"
                                         :options {:endpoint-url "http://mock-api-service:8080/castlemock/mock/soap/project/ZFgj7E/PokemonSOAP"}})
          result (client :GetByIdOperation {:id 1})]
      (is (= #clojure.data.xml.Element{:tag     :GetByIdOperationResponse,
                                       :attrs   {},
                                       :content (#clojure.data.xml.Element{:tag :id, :attrs {}, :content ("1")}
                                                  #clojure.data.xml.Element{:tag :name, :attrs {}, :content ("bulbasaur")}
                                                  #clojure.data.xml.Element{:tag :height, :attrs {}, :content ("7")}
                                                  #clojure.data.xml.Element{:tag :weight, :attrs {}, :content ("69")}
                                                  #clojure.data.xml.Element{:tag :ability, :attrs {}, :content (#clojure.data.xml.Element{:tag :name, :attrs {}, :content ("chlorophyll")})})}
             result)))))

(deftest test-throw-if-method-illegal
  (let [client (soap-client/client-fn {:wsdl wsdl-test})]
    (is (thrown? IllegalArgumentException
                 (client :foo {})))))
